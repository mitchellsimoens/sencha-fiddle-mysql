'use strict';

module.exports = {
    get Adapter () {
        return require('./Adapter');
    },

    get operation () {
        return {
            get fiddle () {
                return {
                    get create () {
                        return require('./operation/fiddle/create');
                    },

                    get get () {
                        return require('./operation/fiddle/get');
                    },

                    get update () {
                        return require('./operation/fiddle/update');
                    },

                    get asset () {
                        return {
                            get create () {
                                return require('./operation/fiddle/asset/create');
                            },

                            get delete () {
                                return require('./operation/fiddle/asset/delete');
                            },

                            get get () {
                                return require('./operation/fiddle/asset/get');
                            },

                            get update () {
                                return require('./operation/fiddle/asset/update');
                            }
                        };
                    },

                    get mockdata () {
                        return {
                            get create () {
                                return require('./operation/fiddle/mockdata/create');
                            },

                            get delete () {
                                return require('./operation/fiddle/mockdata/delete');
                            },

                            get get () {
                                return require('./operation/fiddle/mockdata/get');
                            },

                            get update () {
                                return require('./operation/fiddle/mockdata/update');
                            }
                        };
                    },

                    get pacakge () {
                        return {
                            get create () {
                                return require('./operation/fiddle/package/create');
                            },

                            get delete () {
                                return require('./operation/fiddle/package/delete');
                            },

                            get update () {
                                return require('./operation/fiddle/package/update');
                            }
                        };
                    }
                };
            },

            get framework () {
                return {
                    get get () {
                        return require('./operation/framework/get');
                    },

                    get asset () {
                        return {
                            get get () {
                                return require('./operation/framework/asset/get');
                            }
                        };
                    },

                    get package () {
                        return {
                            get get () {
                                return require('./operation/framework/package/get');
                            },

                            get asset () {
                                return {
                                    get get () {
                                        return require('./operation/framework/package/asset/get');
                                    }
                                };
                            }
                        };
                    }
                };
            },

            get permission () {
                return {
                    get fiddle () {
                        return {
                            get get () {
                                return require('./operation/permission/fiddle/get');
                            }
                        };
                    },

                    get team () {
                        return {
                            get get () {
                                return require('./operation/permission/team/get');
                            }
                        };
                    }
                };
            }
        }
    }
};
