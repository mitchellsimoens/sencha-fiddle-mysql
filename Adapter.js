'use strict';

const { Adapter } = require('sencha-fiddle');

/**
 * @class Sencha.fiddle.mysql.Adapter
 * @extends Sencha.fiddle.Adapter
 *
 * An adapter to hold MySQL operations.
 */
class MySQLAdapter extends Adapter {
    static get meta () {
        return {
            prototype : {
                /**
                 * @readonly
                 * @property {Boolean} [isFiddleMySQLAdapter=true]
                 */
                isFiddleMySQLAdapter : true,

                rootPath : __dirname
            }
        };
    }
}

module.exports = MySQLAdapter;
