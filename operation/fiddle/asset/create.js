'use strict';

const { Base }  = require('sencha-core');
const { Query } = require('sencha-mysql');

/**
 * @class Sencha.fiddle.mysql.operation.fiddle.Asset.create
 * @extends Sencha.core.Base
 *
 * A class to manage all CREATE operations for fiddle assets.
 */
class Asset extends Base {
    static get meta () {
        return {
            prototype : {
                /**
                 * @readonly
                 * @property {Boolean} [isFiddleAssetCreator=true]
                 */
                isFiddleAssetCreator : true
            }
        };
    }

    create (data, frameworkid, batch) {
        return new Promise((resolve, reject) => {
            let query = new Query({
                inserts : [
                    data.name,
                    data.code,
                    data.type,
                    data.statusCode || 200,
                    !!data.remote,
                    frameworkid
                ],
                sqls    : `INSERT INTO fiddle_assets
                    (fiddleid, name, code, type, statusCode, remote, extWrap)
                    SELECT @fiddleid, ?, ?, ?, ?, ?, fiddle_catalog.extWrap FROM fiddle_catalog WHERE id = ? AND @permission > 1;`
            });

            batch.add(query);

            query.then(resolve, reject);
        });
    }
}

module.exports = Asset;
