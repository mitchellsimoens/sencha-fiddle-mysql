'use strict';

const { Base }  = require('sencha-core');
const { Query } = require('sencha-mysql');

/**
 * @class Sencha.fiddle.mysql.operation.Fiddle.create
 * @extends Sencha.core.Base
 *
 * A class to manage all CREATE operations for fiddle assets.
 */
class Fiddle extends Base {
    static get meta () {
        return {
            prototype : {
                /**
                 * @readonly
                 * @property {Boolean} [isFiddleCreator=true]
                 */
                isFiddleCreator : true
            }
        };
    }

    create (data, batch) {
        return new Promise((resolve, reject) => {
            let query = new Query({
                inserts : [
                    data.forkid,
                    data.userid,
                    data.username,
                    data.frameworkid,
                    data.frameworkVersion,
                    data.title,
                    data.description,
                    data.index,
                    data.password ? data.password : '',
                    data.rtl,
                    data.version || 2
                ],
                sqls    : [
                    `INSERT INTO fiddles
                    (\`forkid\`, \`userid\`, \`username\`, \`frameworkid\`, \`frameworkVersion\`, \`createdDate\`, \`modifiedDate\`, \`title\`, \`description\`, \`index\`, \`password\`, \`rtl\`, \`version\`)
                    SELECT ?, ?, ?, ?, ?, NOW(), NOW(), ?, ?, ?, ?, ?, ? FROM (SELECT 1) temp WHERE @permission > 1;`,
                    `SET @fiddleid = LAST_INSERT_ID();`
                ]
            });

            batch.add(query);

            query.then(resolve, reject);
        });
    }

    /**
     * Creates a temporary fiddle adding the assets and mock data
     * to the temporary assets table. This will also delete 50 rows
     * from the temporary assets table.
     * @param {Object} data
     * @param {Array} data.assets The array of assets for the fiddle.
     * @param {Array} data.mockdata The array of mock data for the fiddle.
     * @param {Object} session
     * @param {String} session.id The session id.
     * @param {Sencha.mysql.Batch} batch The batch to add queries to.
     * @return {Promise}
     */
    temp (data, session, batch) {
        return new Promise((resolve, reject) => {
            let now          = new Date(),
                assets       = data.assets,
                mockdata     = data.mockdata,
                framework    = data.framework,
                isDescriptor = typeof framework === 'object',
                inserts      = [
                    session.id
                ],
                sqls         = [
                    'DELETE FROM fiddle_assets_temp WHERE sessionid = ?;',
                    'DELETE FROM fiddle_assets_temp WHERE created <= DATE_SUB(NOW(), INTERVAL 12 HOUR) LIMIT 50;' //TODO make configurable
                ],
                query        = new Query({
                    inserts : inserts,
                    sqls    : sqls
                });

            if (Array.isArray(assets)) {
                for (let asset of assets) {
                    if (isDescriptor) {
                        let wheres = [];

                        if (framework.framework) {
                            inserts.push(framework.framework);
                            wheres.push('fiddle_catalog.framework = ?');
                        }

                        if (framework.theme) {
                            inserts.push(framework.theme);
                            wheres.push('fiddle_catalog.theme = ?');
                        }

                        if (framework.toolkit) {
                            inserts.push(framework.toolkit);
                            wheres.push('fiddle_catalog.toolkit = ?');
                        }

                        if (framework.version) {
                            inserts.push(framework.version);
                            wheres.push(`LEFT(fiddle_catalog.version, ${framework.version.length}) = ?`);
                        }

                        sqls.push(`
                            INSERT INTO fiddle_assets_temp
                            SET
                                extWrap = (SELECT extWrap FROM fiddle_catalog WHERE ${wheres.join(' AND ')} ORDER BY version DESC LIMIT 1),
                                sessionid = ?,
                                created = NOW(),
                                type = ?,
                                url = ?,
                                code = ?,
                                remote = ?;
                        `);

                        inserts.push(session.id, asset.type, asset.name, asset.code, asset.remote);
                    } else {
                        sqls.push('INSERT INTO fiddle_assets_temp SET sessionid = ?, created = NOW(), type = ?, url = ?, code = ?, remote = ?, extWrap = (SELECT extWrap FROM fiddle_catalog WHERE id = ? ORDER BY version DESC LIMIT 1);');

                        inserts.push(session.id, asset.type, asset.name, asset.code, asset.remote, data.framework);
                    }
                }
            }

            if (Array.isArray(mockdata)) {
                for (let asset of mockdata) {
                    if (asset.type === 'direct') {
                        this._parseDirect(session.id, now, asset.children, null, (item) => {
                            sqls.push('INSERT INTO fiddle_assets_temp SET ?;');

                            inserts.push(item);
                        });
                    } else {
                        sqls.push('INSERT INTO fiddle_assets_temp SET ?;');

                        inserts.push({
                            sessionid  : session.id,
                            created    : now,
                            type       : asset.type,
                            url        : asset.url,
                            code       : asset.code,
                            dynamic    : asset.dynamic,
                            delay      : asset.delay,
                            statusCode : asset.statusCode
                        });
                    }
                }
            }

            batch.add(query);

            query.then(resolve, reject);
        });
    }

    /**
     * Parses the `Ext.Direct` assets as they may be in a tree structure.
     * @private
     * @param {String} sessionid The session id.
     * @param {Date} created The date to set the asset created at.
     * @param {Object} data The asset's data.
     * @param {String} root The ending name of the asset.
     * @param {Function} fn The function to execute for each leaf item.
     */
    _parseDirect (sessionid, created, data, root, fn) {
        if (data.type === 'direct') {
            //is the "leaf" object
            fn({
                sessionid   : sessionid,
                created     : created,
                type        : data.type,
                url         : root,
                code        : data.code,
                dynamic     : data.dynamic,
                delay       : data.delay,
                statusCode  : data.statusCode,
                formHandler : data.formHandler,
                direct_len  : data.direct_len,
                direct_args : Array.isArray(data.direct_args) && data.direct_args.length ? data.direct_args.join() : null
            });
        } else {
            for (let key in data) {
                let thisRoot = root ? root + '.' + key : key;

                this._parseDirect(sessionid, created, data[key], thisRoot, fn);
            }
        }
    }
}

module.exports = Fiddle;
